//
//
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
class JobAvaliablePresenter: JobAvaliableViewToPresenterProtocol{
    
    var view: JobAvaliablePresenterToViewProtocol?
    var router: JobAvaliablePresenterToRouterProtocol?
    var interactor: JobAvaliablePresenterToInteratorProtocol?
    
    func started(){
        view?.initialControlSetup()
        view?.defaultUI()
    }
    
    func gotoLoginConfirmScreen(){
        
    }
    
    init(view: JobAvaliablePresenterToViewProtocol, interactor: JobAvaliablePresenterToInteratorProtocol,router: JobAvaliablePresenterToRouterProtocol){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

//MARK: - Interactor to Presenter
extension JobAvaliablePresenter : JobAvaliableInteractorToPresenterProtocol{
    
}
