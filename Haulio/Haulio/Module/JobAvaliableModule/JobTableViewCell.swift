//
//
//  Haulio
//
//  Created by Thaw  zin oo on 18/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import UIKit
import GoogleSignIn

class JobTableViewCell: UITableViewCell {
    
    @IBOutlet weak var jobNumberValue: UILabel!
    @IBOutlet weak var companyNameValue: UILabel!
    @IBOutlet weak var addressValue: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    
    var parent:JobAvaliableView?
    var job:Job!
    
    func configure(_ content:Job){
        self.job =  content
        jobNumberValue.text = String(content.jobNumber)
        companyNameValue.text = content.companyName
        addressValue.text = content.address
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        acceptBtn.layer.cornerRadius = 0.7
    }
    
    override func layoutSubviews() {
        acceptBtn.layer.cornerRadius = 0.7
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func acceptJob(_ sender: UIButton) {
        self.parent?.performSegue(withIdentifier: segue.mapsegue, sender: job )
        //         GIDSignIn.sharedInstance()?.currentUser
    }
    
}
