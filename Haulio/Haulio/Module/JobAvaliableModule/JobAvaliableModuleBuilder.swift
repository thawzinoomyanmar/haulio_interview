//
//
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
import UIKit
class JobAvaliableModuleBuilder{
    static func build() -> UIViewController{
        let storyboard = UIStoryboard.init(name: "", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "") as! JobAvaliableView
        let interactor = JobAvaliableInteractor()
        let router = JobAvaliableRouter(view: view)
        let presenter = JobAvaliablePresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        return view
    }
}
