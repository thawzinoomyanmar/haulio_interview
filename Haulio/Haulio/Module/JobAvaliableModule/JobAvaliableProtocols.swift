//
//
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
import UIKit

//Presenter -> View
protocol JobAvaliablePresenterToViewProtocol: class {
    func initialControlSetup()
    func defaultUI()
}

//View -> Presenter
protocol JobAvaliableViewToPresenterProtocol: class {
    var view: JobAvaliablePresenterToViewProtocol? {get set}
    var interactor: JobAvaliablePresenterToInteratorProtocol? {get set}
    func started()
}

//Interactor -> Presenter
protocol JobAvaliableInteractorToPresenterProtocol: class {
    
}

//Presenter -> Interactor
protocol JobAvaliablePresenterToInteratorProtocol: class{
    var presenter: JobAvaliableInteractorToPresenterProtocol?{get set}
}

//Presenter -> Router or WireFrame
protocol JobAvaliablePresenterToRouterProtocol: class {
    func gotoNextScreen(fromVC: UIViewController,buttonTitle: String)
}
