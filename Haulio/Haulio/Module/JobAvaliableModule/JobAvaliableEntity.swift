//
//
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//


import Foundation
import CoreLocation

class Job{
    let jobNumber:Int
    let companyName:String
    let address:String
    var geolocation:CLLocation?  =  nil
    
    init(jobNumber:Int,companyName:String,address:String){
        self.jobNumber = jobNumber
        self.companyName = companyName
        self.address = address
    }
}

class Profile{
    let userName:String
    let jobID:Int
    init(userName:String,jobID:Int){
        self.jobID  = jobID
        self.userName = userName
    }
}


