//
//
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//


import UIKit
import GoogleSignIn
import GoogleMaps

class JobAvaliableView: UIViewController {
    
    var presenter: JobAvaliableViewToPresenterProtocol?
    @IBOutlet weak var tableView: UITableView!
    
    var googleUser:GIDGoogleUser?
    var locationManager:CLLocationManager!
    var userlocation:CLLocation?
    var reuseIdentifie = "cell"
    var jobs = [Job]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.started()
        setup()
    }
    
    func setup(){
        tableView.delegate = self
        tableView.dataSource = self
        getData()
        addNavBarImage()
        googleUser =  GIDSignIn.sharedInstance()?.currentUser
        locationManager = CLLocationManager()
        locationManager.delegate = self
    }
    
    override func viewDidAppear (_ animated: Bool) {
        //Request Authorization ***
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case   .restricted, .denied:print("Sorry ")
            case .authorizedAlways, .authorizedWhenInUse:
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest // less batery ussage
                self.locationManager.pausesLocationUpdatesAutomatically = false
                self.locationManager.startUpdatingLocation()
            case .notDetermined:  break
            }
        }
    }
    
    
}
extension JobAvaliableView: JobAvaliablePresenterToViewProtocol{
    func initialControlSetup() {
        
    }
    
    func defaultUI(){
        
    }
}
extension JobAvaliableView: UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.userlocation = locations.first
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobs.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentJob = jobs[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifie, for: indexPath) as! JobTableViewCell
        cell.parent = self
        cell.configure(  currentJob)
        return cell
    }
    
    func getData(){
        let manager = APIManager.main.getJob(onSuccess: { (jobDictionaryArray) in
            let jobs = jobDictionaryArray
            for job in jobs {
                let jobID = job["job-id"] as? Int  ?? 0
                let company = job["company"] as? String  ?? "Unknown"
                let addr = job["address"] as? String  ?? "Unknown"
                let location =  job["geolocation"] as? [String:Double] ?? [:]
                let locationLat = location ["latitude"] as? Double ??  0.0
                let locationLong = location ["longitude"] as? Double  ?? 0.0
                let coordinate  =   CLLocation(latitude:  locationLat,longitude:locationLong)
                let job = Job(jobNumber: jobID, companyName: company, address:addr)
                job.geolocation =  coordinate
                self.jobs.append(job)
            }
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func addNavBarImage() {
        let navController = navigationController!
        let image = UIImage(named: "setTitle")
        let imageView = UIImageView(image: image)
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .center
        navigationItem.titleView = imageView
        let logOutImage = UIImage(named: "logout")
        let imageLogOut = UIImageView(image: logOutImage)
        let logOutBtn = UIBarButtonItem(image: logOutImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(logout))
        self.navigationItem.rightBarButtonItem = logOutBtn
    }
    
    @objc func logout(_ sender:UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch  segue.identifier {
        case  "mapsegue": let destVC = segue.destination as? MapView
        destVC?.job =  sender as? Job
        destVC?.currentUserLocation = self.userlocation
        default: ()
            
        }
    }
}

