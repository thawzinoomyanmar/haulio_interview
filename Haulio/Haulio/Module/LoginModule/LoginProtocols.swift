//
//  LoginProtocols.swift
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
import UIKit

//Presenter -> View
protocol LoginPresenterToViewProtocol: class {
    func initialControlSetup()
    func defaultUI()
}

//View -> Presenter
protocol LoginViewToPresenterProtocol: class {
    var view: LoginPresenterToViewProtocol? {get set}
    var interactor: LoginPresenterToInteratorProtocol? {get set}
    func started()
}

//Interactor -> Presenter
protocol LoginInteractorToPresenterProtocol: class {
    
}

//Presenter _> Interactor
protocol LoginPresenterToInteratorProtocol: class{
    var presenter: LoginInteractorToPresenterProtocol?{get set}
}

//Presenter -> Router or WireFrame
protocol LoginPresenterToRouterProtocol: class {
    func gotoNextScreen(fromVC: UIViewController,buttonTitle: String)
}
