//
//  LoginListView.swift
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
import UIKit
import GoogleSignIn

class LoginView: UIViewController {
    
    var presenter: LoginViewToPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.started()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if CheckInternet.Connection(){
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate =  self
        }
        else{
            
            self.Alert(Message: Constants.noConnection)
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
}

extension LoginView: LoginPresenterToViewProtocol{
    func initialControlSetup() {
        defaultUI()
    }
    
    func defaultUI(){
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate =  self
    }
}

extension LoginView: GIDSignInDelegate,GIDSignInUIDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        print("Login success")
        performSegue(withIdentifier: segue.jobsegue, sender: user)
    }
    
    func Alert (Message: String){
        
        let alert = UIAlertController(title: Constants.label, message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC = segue.destination as? JobAvaliableView
    }
}
