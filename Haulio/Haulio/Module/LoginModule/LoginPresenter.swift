//
//  LoginPresenter.swift
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
class LoginPresenter: LoginViewToPresenterProtocol{
    var view: LoginPresenterToViewProtocol?
    var router: LoginPresenterToRouterProtocol?
    var interactor: LoginPresenterToInteratorProtocol?
    
    func started(){
        view?.initialControlSetup()
        view?.defaultUI()
    }
    
    func gotoLoginConfirmScreen(){
        
    }
    
    init(view: LoginPresenterToViewProtocol, interactor: LoginPresenterToInteratorProtocol,router: LoginPresenterToRouterProtocol){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

//MARK: - Interactor to Presenter
extension LoginPresenter : LoginInteractorToPresenterProtocol{

}
