//
//  LoginWireFrame.swift
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//
import UIKit
import Foundation

class LoginRouter{
    var viewController: UIViewController
    init(view: UIViewController) {
        self.viewController = view
    }
}

//MARK: - Presenter to Wireframe Interface
extension LoginRouter: LoginPresenterToRouterProtocol{
    func gotoNextScreen(fromVC: UIViewController, buttonTitle: String) {
        
    }
}
