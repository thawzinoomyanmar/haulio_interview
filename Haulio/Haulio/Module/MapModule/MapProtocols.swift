//
//
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
import UIKit

//Presenter -> View
protocol MapPresenterToViewProtocol: class {
    func initialControlSetup()
    func defaultUI()
}

//View -> Presenter
protocol MapViewToPresenterProtocol: class {
    var view: MapPresenterToViewProtocol? {get set}
    var interactor: MapPresenterToInteratorProtocol? {get set}
    func started()
}

//Interactor -> Presenter
protocol MapInteractorToPresenterProtocol: class {
    
}

//Presenter _> Interactor
protocol MapPresenterToInteratorProtocol: class{
    var presenter: MapInteractorToPresenterProtocol?{get set}
}

//Presenter -> Router or WireFrame
protocol MapPresenterToRouterProtocol: class {
    func gotoNextScreen(fromVC: UIViewController,buttonTitle: String)
}
