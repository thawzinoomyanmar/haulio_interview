//
//
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import UIKit
import  GoogleSignIn
import GoogleMaps
import GooglePlaces
import Kingfisher
import CoreLocation

class MapView: UIViewController {
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userJobID: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblPlaces: UITableView!
    var presenter: MapViewToPresenterProtocol?
    var resultsArray:[Dictionary<String, AnyObject>] = Array()
    var mapPlaceAutoCompleteVC:GMSAutocompleteViewController = GMSAutocompleteViewController ( )
    var job:Job!
    var currentUserLocation:CLLocation?
    var user:GIDGoogleUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.started()
        defaultUI()
    }
    
    func getNavImage(){
        let navController = navigationController!
        let image = UIImage(named: "setTitle")
        let imageView = UIImageView(image: image)
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .left
        navigationItem.titleView = imageView
        let logOutImage = UIImage(named: "logout")
        let imageLogOut = UIImageView(image: logOutImage)
        let logOutBtn = UIBarButtonItem(image: logOutImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(logout))
        self.navigationItem.rightBarButtonItem = logOutBtn
        
    }
    
    @objc func logout(_ sender:UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
extension MapView: MapPresenterToViewProtocol{
    func initialControlSetup() {
        defaultUI()
    }
    func defaultUI(){
        getNavImage()
        user =  GIDSignIn.sharedInstance()?.currentUser
        userName.text = user?.profile.givenName ?? "Unknown"
        let url =  user?.profile.imageURL(withDimension: 200)
        profileImage.kf.setImage(with: url)
        profileImage.kf.indicatorType = .activity
        profileImage.layer.cornerRadius =  profileImage.layer.frame.size.width /  2
        profileImage.clipsToBounds = true
        userJobID.text =  String(job?.jobNumber ?? 0)
        setLocation( job.geolocation?.coordinate)
        setUserLocation( )
        mapPlaceAutoCompleteVC.delegate = self
        mapPlaceAutoCompleteVC.view.backgroundColor = UIColor.clear
        txtSearch.addTarget(self, action: #selector(searchPlaceFromGoogle(_:)), for: .editingChanged)
        tblPlaces.estimatedRowHeight = 44.0
        tblPlaces.dataSource = self
        tblPlaces.delegate = self
    }
}

extension MapView: GMSAutocompleteViewControllerDelegate,UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "placecell")
        if let lblPlaceName = cell?.contentView.viewWithTag(102) as? UILabel {
            
            let place = self.resultsArray[indexPath.row]
            lblPlaceName.text = "\(place["name"] as! String) \(place["formatted_address"] as! String)"
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let place = self.resultsArray[indexPath.row]
        if let locationGeometry = place["geometry"] as? Dictionary<String, AnyObject> {
            if let location = locationGeometry["location"] as? Dictionary<String, AnyObject> {
                if let latitude = location["lat"] as? Double {
                    if let longitude = location["lng"] as? Double {
                        UIApplication.shared.open(URL(string: "https://www.google.com/maps/@\(latitude),\(longitude),16z")!, options: [:])
                    }
                }
            }
        }
    }
    
    @objc func searchPlaceFromGoogle(_ textField:UITextField) {
        if let searchQuery = textField.text {
            var strGoogleApi = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(searchQuery)&key= AIzaSyABWLJXK3XYoNT3-1A7yQmXMLuu03Oln_Y"
            strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
                if error == nil {
                    
                    if let responseData = data {
                        let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        if let dict = jsonDict as? Dictionary<String, AnyObject>{
                            if let results = dict["results"] as? [Dictionary<String, AnyObject>] {
                                print("json == \(results)")
                                self.resultsArray.removeAll()
                                for dct in results {
                                    self.resultsArray.append(dct)
                                }
                                DispatchQueue.main.async {
                                    self.tblPlaces.reloadData()
                                }
                            }
                        }
                        
                    }
                } else {
                    //we have error connection google api
                }
            }
            task.resume()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setLocation(_  position:CLLocationCoordinate2D?) {
        if let position = position {
            let marker = GMSMarker()
            marker.position = position
            marker.map = self.mapView
            mapView.animate(toZoom: 13.0)
            mapView.animate(toLocation: position)
        }
    }
    
    func setUserLocation() {
        if let position = currentUserLocation?.coordinate {
            let marker = GMSMarker()
            marker.position = position
            marker.map = self.mapView
            mapView.animate(toZoom: 13.0)
            mapView.animate(toLocation: position)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.present(   mapPlaceAutoCompleteVC, animated: true)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.mapPlaceAutoCompleteVC.dismiss(animated: true) {
            let location = place.coordinate
            self.setLocation(location)
        }
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.mapPlaceAutoCompleteVC.dismiss(animated: true)
    }
}
