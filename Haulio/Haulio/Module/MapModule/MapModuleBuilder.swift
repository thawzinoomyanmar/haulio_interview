//
//  
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
import UIKit
class MapModuleBuilder{
    static func build() -> UIViewController{
        let storyboard = UIStoryboard.init(name: "", bundle: nil)
           let view = storyboard.instantiateViewController(withIdentifier: "") as! MapView
        let interactor = MapInteractor()
        let router = MapRouter(view: view)
        let presenter = MapPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        return view
    }
}
