//
//
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//
import UIKit
import Foundation

class MapRouter{
    var viewController: UIViewController
    init(view: UIViewController) {
        self.viewController = view
    }
}

//MARK: - Presenter to Wireframe Interface
extension MapRouter: MapPresenterToRouterProtocol{
    func gotoNextScreen(fromVC: UIViewController, buttonTitle: String) {
        //
    }
}
