//
//  
//  Haulio
//
//  Created by Thaw  zin oo on 19/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
class MapPresenter: MapViewToPresenterProtocol{
    var view: MapPresenterToViewProtocol?
    var router: MapPresenterToRouterProtocol?
    var interactor: MapPresenterToInteratorProtocol?
    
    func started(){
        view?.initialControlSetup()
        view?.defaultUI()
    }
    
    func gotoLoginConfirmScreen(){
        
    }
    init(view: MapPresenterToViewProtocol, interactor: MapPresenterToInteratorProtocol,router: MapPresenterToRouterProtocol){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

//MARK: - Interactor to Presenter
extension MapPresenter : MapInteractorToPresenterProtocol{

}
