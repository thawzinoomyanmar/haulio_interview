//
//  Constants.swift
//  Haulio
//
//  Created by Thaw  zin oo on 18/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation

struct Constants {
    static let baseURL = "https://www.haulio.io"
    static let URL = "https://www.google.com"
    static let noConnection = "Your Device is not connected with internet.Please check your internet connection"
    static let  label = "No Internet Connection"
}

struct segue {
    static let mapsegue = "mapsegue"
    static let jobsegue = "jobsegue"
}
