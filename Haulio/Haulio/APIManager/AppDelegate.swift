//
//  AppDelegate.swift
//  Haulio
//
//  Created by Thaw  zin oo on 18/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        // Override point for customization after application launch.
        
        //Google SignIn
        GIDSignIn.sharedInstance().clientID = "1018113755382-8kva4mucqo5envvue24j8k377bdhunv4.apps.googleusercontent.com"
        
        //Google Map Setup
        ////THIRD PARTY
        GMSServices.provideAPIKey("AIzaSyCD4aUmyRQ0WSHP510KPQnWNDMcs9LatPI")
        GMSPlacesClient.provideAPIKey("AIzaSyAOdQdaBsZE8Rcuj-9Skq6nmfrvEU16WZw")
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

