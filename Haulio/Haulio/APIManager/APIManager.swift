//
//  APIManager.swift
//  Haulio
//
//  Created by Thaw  zin oo on 18/5/20.
//  Copyright © 2020 Thaw  zin oo. All rights reserved.
//

import Foundation
import Alamofire

class APIManager:NSObject {
    
    static var main = APIManager()
    static let baseURLString = Constants.baseURL

    private enum EndPoint: String {
        //Registration & Sign In
        case jobdata = "/joblist.json"
        var url:URL  {
            //Forcely return as of all API end Points are confirmed
            return URL(string: APIManager.baseURLString + rawValue) ?? URL(string:Constants.URL)!
        }
        var method: HTTPMethod {
            switch self {
            case .jobdata: return .get
            default:return .post  //TODO: Check all request are set as Post, unless specified
            }
        }
    }
    
    func getJob( onSuccess:(([[String:Any]] )->())? = nil , onError:((Error)->())? = nil  ) {
        var parameters:Parameters = [:]
        AF.request(EndPoint.jobdata.url, method:EndPoint.jobdata.method, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response ) in
            if let error = response.error {
                onError?(error)
            } else {
                if let responseDict =  response.value as? [[String:Any]] {
                    onSuccess?(responseDict)
                }
                
            }
        }
    }
}

